import selenium
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup
from datetime import datetime
import requests
import os
import sys
import re
from time import sleep
from zipfile import ZipFile
from model import Model
import configparser
import win32event
import win32serviceutil
import win32service
import servicemanager
import winreg as reg


class ParserVpn(win32serviceutil.ServiceFramework):
    _svc_name_ = 'ParserVpn'
    _svc_display_name_ = 'ParserVpn'
    _svc_description_ = 'Parser vpn get free openvpn configs'

    def __init__(self, args):
        win32serviceutil.ServiceFramework.__init__(self, args)
        self.hWaitStop = win32event.CreateEvent(None, 0, 0, None)
        self.isAlive = True

        with reg.OpenKey(reg.HKEY_LOCAL_MACHINE, "SYSTEM\\CurrentControlSet\\Services\\ParserVpn") as h:
            self.path = reg.EnumValue(h, 3)[1].strip('""').strip("vpnparser.exe")



        self.config = configparser.ConfigParser()
        self.config.read(self.path + 'config.ini', encoding='utf-8')

        self.model = Model(self.config)
        options = Options()
        options.add_argument("-headless")
        # options.add_argument("start-maximized") #open Browser in maximized mode
        # options.add_argument("disable-infobars") # disabling infobars
        # options.add_argument("--disable-extensions"); # disabling extensions
        # options.add_argument("--disable-gpu") # applicable to windows os only
        # options.add_argument("--disable-dev-shm-usage") # overcome limited resource problems
        # options.add_argument("--no-sandbox") # Bypass OS security model

        self.driver = selenium.webdriver.Chrome(options=options)

    # vpnbook.com
    def get_vpnbook_configs(self):

        url = r"https://www.vpnbook.com/"
        self.driver.get(url)
        sleep(2)
        soup = BeautifulSoup(self.driver.page_source, 'html5lib')
        list_config = soup.select('li#openvpn ul')[0].findAll('li')

        # Get login and passowrd

        # get login
        login = soup.find_all('strong', string=re.compile('Username:'))[0].contents[0].replace("Username:", "")

        # get password image
        url_image_pass = url + list_config[0].find_all_next('strong')[0].find_next('img').attrs['src']
        response = requests.get(url_image_pass)

        os.chdir(self.path)

        # Create directories if not exist
        if not os.path.isdir('temp'):
            os.mkdir('temp')
        if not os.path.isdir('configs'):
            os.mkdir('configs')

        # save password to file
        os.chdir(self.path + 'temp')
        password_image = 'password.png'
        with open(password_image, 'wb') as fi:
            fi.write(response.content)



        # get password from image
        os.chdir(self.path)
        import pytesseract
        pytesseract.pytesseract.tesseract_cmd = 'Tesseract-OCR\\tesseract.exe'
        from PIL import Image
        value = Image.open('temp\\' + password_image)
        password = pytesseract.image_to_string(value).replace('\n\n','')

        # get links file
        links = list_config[0].find_all_next('a')

        # write files
        for link in links:
            response = requests.get(url + link.attrs['href'])
            m = re.search(r'(?<=\/)[^\/\\:*?"<>|\n]+\.(?:zip)$', link.attrs['href'])
            try:
                file_name = m.group(0)

                # save zip files
                with open(self.path + 'temp\\' + file_name, 'wb') as f:

                    f.write(response.content)
            except Exception:
                pass


        # extract file
        zip_files = os.listdir(self.path + 'temp')

        for file in zip_files:
            try:
                with ZipFile(os.path.abspath(self.path + 'temp') + '\\' + file) as zf:
                    zf.extractall(path=os.path.abspath(self.path + 'configs'))
            except Exception:
                pass

        config_filter = [
            'vpnbook-ca149-tcp443.ovpn',
            'vpnbook-ca196-tcp443.ovpn',
            'vpnbook-de20-tcp443.ovpn',
            'vpnbook-de220-tcp443.ovpn',
            'vpnbook-fr200-tcp443.ovpn',
            'vpnbook-fr231-tcp443.ovpn',
            'vpnbook-pl134-tcp443.ovpn',
            'vpnbook-uk68-tcp443.ovpn',
            'vpnbook-uk205-tcp443.ovpn',
            'vpnbook-us1-tcp443.ovpn',
            'vpnbook-us2-tcp443.ovpn',
        ]

        def filter_configs(x):
            if x in config_filter:
                return os.path.join(self.path + 'configs', x)

        def get_files(root_dir):

            for root, dirs, f in os.walk(root_dir):

                files = list(map(filter_configs, f))

                for d in dirs:
                    dir = os.path.join(root, d)
                    for file in os.scandir(dir):
                        if file.name in config_filter:
                            files.append(file.path)

                return files


        files = get_files(self.path + 'configs')
        for index, f in enumerate(files):
            if f != None:
                with open(f, 'rb') as f:
                    file = f.read()
                id = "vpnbook-" + str(index)
                self.model.set_config(id=id, login=login, password=password, file=file)

    def main(self):
        while self.isAlive:

            if self.config['start']['time'] == datetime.now().strftime("%H:%M"):
                # delete all configs
                self.model.delete_configs()

                # get vpn configs from vpnbook.com
                self.get_vpnbook_configs()

        sleep(60)

    def SvcDoRun(self):
        self.isAlive = True
        self.ReportServiceStatus(win32service.SERVICE_START_PENDING)
        self.ReportServiceStatus(win32service.SERVICE_RUNNING)
        self.main()
        win32event.WaitForSingleObject(self.hWaitStop, win32event.INFINITE)

    def SvcStop(self):
        self.isAlive = False
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        win32event.SetEvent(self.hWaitStop)


if __name__ == '__main__':
    if len(sys.argv) == 1:
        servicemanager.Initialize()
        servicemanager.PrepareToHostSingle(ParserVpn)
        servicemanager.StartServiceCtrlDispatcher()
    else:
        win32serviceutil.HandleCommandLine(ParserVpn)
