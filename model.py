import psycopg2
from sshtunnel import *


class Model(object):
    _instance = None

    def __new__(cls, config):
        if cls._instance is None:
           cls._instance = super(Model, cls).__new__(cls)
        return cls._instance

    def __init__(self, config):

        try:
            server = open_tunnel(
                (config['ssh']['hostname'], int(config['ssh']['port'])),
                ssh_username=config['ssh']['username'],
                ssh_password=config['ssh']['password'],
                remote_bind_address=(config['database']['hostname'], int(config['database']['port'])))
            server.start()

            self.conn = psycopg2.connect(
                database=config['database']['dbname'],
                user=config['database']['username'],
                password=config['database']['password'],
                host=config['database']['hostname'],
                port=server.local_bind_port,)
            self.cursor = self.conn.cursor()
        except Exception as e:
            pass


    def get_config(self, edrpo):

       self.cursor.execute('SELECT file, login,password FROM config WHERE edrpo = %s ', (edrpo,))
       data = self.cursor.fetchone()
       print(data)
       return data


    def set_config(self, **config):

        try:

            self.cursor.execute("INSERT INTO config(id, file, login, password ) VALUES (%s,%s,%s,%s)",
                           (config['id'],
                            config['file'],
                            config['login'],
                            config['password'],))

            self.conn.commit()

        except Exception as e:
            print(e)


    def delete_configs(self):
        self.cursor.execute("DELETE FROM config")